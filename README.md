# C ve JSON ile Cihaz Veri �stasyonu

C dilini kullanarak d��ar�daki bir cihazdan ald���m�z sens�r verilerini JSON format�na getirip daha sonra i�lem yap�labilir hale getirmek istiyorum. Bu y�zden ilk ad�m olarak verileri d�zg�n bir �ekilde JSON format�na d�n��t�rmeye �al���yorum. 

Bunun i�in cJSON k�t�phanesini kullan�yorum. �rnek kodlar� incelerseniz kullan�m hakk�nda bilgi edinebilirsiniz. 
# sensorler.cpp'in ��kt�s�:

```sh
[
	{
		"device_id": "586e64955cae9e5c54043031",
		"s": [
			{
				"ix": 1,
				"bt": 31,
				"v": {
					"v": 94,
					"t": 27
				}
			},
			{
				"ix": 2,
				"bt": 27,
				"v": {
					"v": 111,
					"t": 2
				}
			},
			{
				"ix": 3,
				"bt": 41,
				"v": {
					"v": 118,
					"t": 38
				}
			}
		]
	},
	{
		"device_id": "12354e6491231e1351231031",
		"s": [
			{
				"ix": 1,
				"bt": 26,
				"v": {
					"v": 138,
					"t": 1
				}
			},
			{
				"ix": 2,
				"bt": 93,
				"v": {
					"v": 97,
					"t": 15
				}
			},
			{
				"ix": 3,
				"bt": 53,
				"v": {
					"v": 103,
					"t": 25
				}
			}
		]
	}
]
```