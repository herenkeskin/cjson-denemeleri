#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "cJSON.h"

// Structure Variables
struct record {
	int ix;
	int bt;
	int v;
	int t;
};

struct device {

	char *device_id;
	struct record sensorInfo[50];

};

int random_range(int min, int max) {
	return (rand() % (max - min) + min);
}

int main(void) {

	struct device devicesInfo[100];
	srand(time(NULL));

	// JSON Variables
	cJSON   *root = NULL,
		*id = NULL,
		*sensorSubValue = NULL,
		*sensorValue = NULL,
		*sensors = NULL,
		*devices = NULL;

	int i, j;

	devicesInfo[0].device_id = "586e64955cae9e5c54043031";
	devicesInfo[1].device_id = "12354e6491231e1351231031";

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 3; j++) {
			devicesInfo[i].sensorInfo[j].ix = j + 1;
			devicesInfo[i].sensorInfo[j].bt = random_range(10, 100);
			devicesInfo[i].sensorInfo[j].v = random_range(50, 250);
			devicesInfo[i].sensorInfo[j].t = random_range(1, 50);
		}
	}

	// Generating JSON
	root = cJSON_CreateArray();

	for (j = 0; j < 2; j++) {
		cJSON_AddItemToArray(root, devices = cJSON_CreateObject());
		cJSON_AddItemToObject(devices, "device_id", id = cJSON_CreateString(devicesInfo[j].device_id));
		cJSON_AddItemToObject(devices, "s", sensors = cJSON_CreateArray());

		for (i = 0; i < 3; i++) {
			cJSON_AddItemToArray(sensors, sensorValue = cJSON_CreateObject());
			cJSON_AddNumberToObject(sensorValue, "ix", devicesInfo[j].sensorInfo[i].ix);
			cJSON_AddNumberToObject(sensorValue, "bt", devicesInfo[j].sensorInfo[i].bt);
			cJSON_AddItemToObject(sensorValue, "v", sensorSubValue = cJSON_CreateObject());
			cJSON_AddNumberToObject(sensorSubValue, "v", devicesInfo[j].sensorInfo[i].v);
			cJSON_AddNumberToObject(sensorSubValue, "t", devicesInfo[j].sensorInfo[i].t);
		}
	}

	printf(cJSON_Print(root));
	cJSON_Delete(root);

	getch();
	return 0;
}