#include <stdio.h>
#include <conio.h>
#include "cJSON.h"

// Structure Variables
struct record {
	const char *device_id;
	const int ix;
	const int bt;
	const int v;
	const int t;
};

// Structure Values
struct record fields[2] = {
	{
		"586e64955cae9e5c54043031",
		24,
		156,
		65,
		0
	},
	{
		"12312364955c1312343031",
		25,
		126,
		15,
		17
	}
};

int main(void) {

	// JSON Variables
	cJSON   *root = NULL, 
			*id = NULL, 
			*sensorSubValue = NULL, 
			*sensorValue = NULL,
			*sensors = NULL, 
			*devices = NULL;

	int i, j;

	// Generating JSON
	root = cJSON_CreateArray();

	for (j = 0; j < 2; j++) {
		cJSON_AddItemToArray(root, devices = cJSON_CreateObject());
		cJSON_AddItemToObject(devices, "device_id", id = cJSON_CreateString(fields[j].device_id));
		cJSON_AddItemToObject(devices, "s", sensors = cJSON_CreateArray());

		for (i = 0; i < 2; i++) {
			cJSON_AddItemToArray(sensors, sensorValue = cJSON_CreateObject());
			cJSON_AddNumberToObject(sensorValue, "ix", fields[i].ix);
			cJSON_AddNumberToObject(sensorValue, "bt", fields[i].bt);
			cJSON_AddItemToObject(sensorValue, "v", sensorSubValue = cJSON_CreateObject());
			cJSON_AddNumberToObject(sensorSubValue, "v", fields[i].v);
			cJSON_AddNumberToObject(sensorSubValue, "t", fields[i].t);
		}
	}

	printf(cJSON_Print(root));
	cJSON_Delete(root);

	getch();
	return 0;
}